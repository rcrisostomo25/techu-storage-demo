function guardar() {
    let clave = document.querySelector('#clave').value;
    let valor = document.querySelector('#valor').value;
    const objeto = { clave, valor};
    sessionStorage.setItem(clave, JSON.stringify(objeto));
    actualizarLista(clave);
}

function actualizarLista(clave) {
    const group = document.querySelector('#group');
    let item = document.createElement('li');
    item.className  = 'list-group-item';
    item.innerHTML = sessionStorage.getItem(clave);
    let button = document.createElement('button');
    button.id = clave;
    button.className = 'btn btn-danger';
    button.innerHTML = 'Eliminar';
    button.onclick = eliminar;
    item.append(' ');
    item.append(button);
    group.append(item);
    getSizeSession();
    document.querySelector('#clave').value = '';
    document.querySelector('#valor').value = '';
}

function limpiar() {
    const group = document.querySelector('#group');
    group.innerHTML = '';
    sessionStorage.clear();
    getSizeSession();
}

function getSizeSession() {
    let size = document.querySelector('#size');
    size.innerHTML = sessionStorage.length;
}

function eliminar(event) {
    var node = event.target;
    if (node.parentNode && node.parentNode.parentNode) {
        node.parentNode.parentNode.removeChild(node.parentNode);
        sessionStorage.removeItem(event.target.id);
        getSizeSession();
    }
}